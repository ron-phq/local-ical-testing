from datetime import timedelta

from dateutil import parser
from icalendar import Event
from rfc3339 import parse_date


def _to_ical_str(event):
    """
    Strips and cleans few details from the icalendar.to_ical function
    to keep the raw ical string.
    """
    s = event.to_ical().decode()

    # to_ical adds newlines within long rrules, including in the middle of the props
    # e.g. RRULE:FREQ=YEARLY;WKS\r\n T=MO;INTERVAL=2; (WKST gets splitted, notice the one space indentation)
    # we remove this here but we might want to include nicer newlines (after a prop) for long rrules in the future?
    # e.g. RRULE:FREQ=YEARLY;WKST=MO;\n INTERVAL=2; (with a one space indentation after the newline)
    # this replacement is different from the next one which removes the carriage returns
    s = s.replace("\r\n ", "")

    s = s.replace("\r\n", "\n").strip()
    return s.split("BEGIN:VEVENT\n")[1].split("\nEND:VEVENT")[0]


def _parse_datetime(dts):
    """
    Parses a Date string or a LocalDateTime string
    and returns the corresponding date or a datetime object.
    """
    try:
        return parse_date(dts)
    except ValueError:
        # should be a datetime then
        pass

    dt = parser.parse(dts)
    assert dt.tzinfo is None, f"Expected a local datetime but found a timezone when parsing: {dts}"

    return dt


def to_ical(recurring):
    """
    Expects a recurring object in the form
        recurring: Option(RecurringDetails)
            dtstart: Date|LocalDateTime
            duration: Option(Duration)
                weeks: Option(Int)
                days: Option(Int)
                hours: Option(Int)
                minutes: Option(Int)
                seconds: Option(Int)
            rrule: RecurrenceRule
                freq: yearly|monthly|weekly|daily|hourly|minutely|secondly
                interval: Option(Int)
                until: Option(Date|LocalDateTime)  # and same value type as dtstart
                ...  # Most rrule fields supported.
            rdates: Option([Date|LocalDateTime])
            exdates: Option([Date|LocalDateTime])

    and returns an icalendar string in the form
        DTSTART:...\n
        DURATION:...\n
        RRULE:...\n
        RDATES:...\n
        EXDATES:...
    """
    event = Event()

    dtstart = _parse_datetime(recurring["dtstart"])
    event.add("DTSTART", dtstart)

    rrule = recurring["rrule"].copy()
    until = rrule.get("until")
    if until:
        until = _parse_datetime(until)

        if type(dtstart) is not type(until):
            raise ValueError(f"dtstart ({dtstart}) and until ({until}) have different value types")

        rrule["until"] = until
    event.add("RRULE", rrule)

    duration = recurring.get("duration")
    if duration:
        event.add("DURATION", timedelta(**duration))

    extra_dt_props = [("rdates", "RDATE"), ("exdates", "EXDATE")]
    for prop, rfc_prop in extra_dt_props:
        vals = recurring.get(prop)
        if vals:
            vals = [_parse_datetime(dt) for dt in vals]
            event.add(rfc_prop, vals)

    return _to_ical_str(event)
