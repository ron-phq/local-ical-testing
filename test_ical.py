from utils.recurring import to_ical


# In entity_normaliser.common_formatter we have this logic:
#    recurring = entity.get("recurring")
#    if recurring:
#        entity["recurring"]["ical"] = to_ical(recurring)

# So if we try this, we'll simulate what happens when the normaliser tries to parse the
# recurring object. If this succeeds, then we can be confident the normaliser will
# handle it properly, and if it causes an error, then we need to fix the recurring
# object's format until it can be parsed successfully.
input_ical_obj = {}
result = to_ical(input_ical_obj)
print(result)